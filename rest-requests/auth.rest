### Create a new user passing email and password
POST http://localhost:3000/auth/signup
content-type: application/json

{
  "email": "admin@gmail.com",
  "password": "admin123",
  "admin": true
}

### Login to the sisten passing email and password
POST http://localhost:3000/auth/signin
content-type: application/json

{
  "email": "admin@gmail.com",
  "password": "admin123"
}

### Signing out from application
POST http://localhost:3000/auth/sign-out

### Return logged user
GET http://localhost:3000/auth/whoami

### Find a particular user with a given id
GET http://localhost:3000/auth/1

### Find all users with a given email
GET http://localhost:3000/auth?email=ivo@gmail.com

### Delete a particular user with a given id
DELETE http://localhost:3000/auth/2

### Updates a particular user with a given id
PATCH http://localhost:3000/auth/1
content-type: application/json

{
  "email": "ivo@gmail.com",
  "password": "SomePass123"
}