import {
  CallHandler,
  ExecutionContext,
  NestInterceptor,
  UseInterceptors,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { map, Observable } from 'rxjs';

//This interface might be used instead of any in this file, to prevent users to enter strings/numbers
//while creating decorators. We are not able to provide more strict restrictions.
//interface ClassConstructor {
//  new (...args: any[]): {}
//}

export function Serialize(dto: any) {
  return UseInterceptors(new SerializeInterceptor(dto));
}

export class SerializeInterceptor implements NestInterceptor {
  constructor(private dto: any) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> {
    //Here run something before a request is handled by a request handler
    return next.handle().pipe(
      map((data: any) => {
        //Here run something before the response is sent out
        return plainToClass(this.dto, data, { excludeExtraneousValues: true });
      }),
    );
  }
}
