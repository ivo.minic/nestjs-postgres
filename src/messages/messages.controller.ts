import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  NotFoundException,
} from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { MessagesService } from './messages.service';

@Controller('messages')
export class MessagesController {
  constructor(public messagesService: MessagesService) {}

  @Get()
  listMessages() {
    return this.messagesService.findAll();
  }

  @Post()
  createMessage(@Body() body: CreateMessageDto) {
    //console.log('Body', body);
    return this.messagesService.createOne(body.content);
  }

  @Get('/:id')
  async getMessage(@Param('id') id: any) {
    //console.log('Id', id);
    const message = await this.messagesService.findOne(id);
    if (message) {
      return message;
    } else {
      throw new NotFoundException('Message not found.');
    }
  }
}
