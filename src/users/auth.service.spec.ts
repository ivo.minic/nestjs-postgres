import { BadRequestException, NotFoundException } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { Users } from './users.entity';
import { UsersService } from './users.service';

describe('AuthService', () => {
  let service: AuthService;
  let fakeUsersService: Partial<UsersService>;

  beforeEach(async () => {
    const users: Users[] = [];
    //Create a fake copy of a usersService
    fakeUsersService = {
      //find: () => Promise.resolve([]),
      find: (email) => {
        const filteredUsers = users.filter((user) => user.email === email);
        return Promise.resolve(filteredUsers);
      },
      //create: (email: string, password: string) =>
      //  Promise.resolve({ id: 1, email, password } as Users),
      create: (email: string, password: string) => {
        const id = Math.floor(Math.random() * 9999999);
        const user = { id, email, password } as Users;
        users.push(user);
        return Promise.resolve(user);
      },
    };
    const module = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: fakeUsersService,
        },
      ],
    }).compile();

    service = module.get(AuthService);
  });

  it('Can create an instance of the auth service', async () => {
    expect(service).toBeDefined();
  });

  it('Creates a new user with salted and hashed password', async () => {
    const user = await service.signup('test@test.com', 'test123');

    expect(user.password).not.toEqual('test123');
    const [salt, hash] = user.password.split('.');
    expect(salt).toBeDefined();
    expect(hash).toBeDefined();
  });

  it('Throws an error if try tu sign up with existing user', async () => {
    await service.signup('test@test.com', 'test123');
    await expect(service.signup('test@test.com', 'test123')).rejects.toThrow(
      BadRequestException,
    );
  });

  it('Throws if signin is called with an unused email', async () => {
    await expect(service.signin('test@test.com', 'test123')).rejects.toThrow(
      NotFoundException,
    );
  });

  it('Throws if an invalid password is provided', async () => {
    await service.signup('test@test.com', 'test123');
    await expect(service.signin('test@test.com', 'password')).rejects.toThrow(
      BadRequestException,
    );
  });

  it('Returns user if correct password is supplied', async () => {
    await service.signup('test@test.com', 'test123');
    const user = await service.signin('test@test.com', 'test123');
    expect(user).toBeDefined();
  });
});
