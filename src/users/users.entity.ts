import { Reports } from 'src/reports/reports.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  AfterInsert,
  AfterUpdate,
  AfterRemove,
  OneToMany,
} from 'typeorm';
//import { Exclude } from 'class-transformer';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  //@Exclude()
  @Column()
  password: string;

  @Column({ default: false })
  admin: boolean;

  @OneToMany(() => Reports, (report) => report.user)
  reports: Reports[];

  @AfterInsert()
  logInsert() {
    console.log('Created user with id:' + this.id);
  }

  @AfterUpdate()
  logUpdate() {
    console.log('Updated user with id:' + this.id);
  }

  @AfterRemove()
  logRemove() {
    console.log('Deleted user with id:' + this.id);
  }
}
