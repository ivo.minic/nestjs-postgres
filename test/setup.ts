import { rm } from 'fs/promises';
import { join } from 'path';

global.beforeEach(async () => {
  try {
    await rm(join(__dirname, '..', 'data', 'test.sqlite'));
  } catch (error) {} //It can throw an error if file does not exist. Don't care about that error.
});
